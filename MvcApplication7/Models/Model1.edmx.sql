
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/15/2015 09:44:32
-- Generated from EDMX file: c:\users\chica\documents\visual studio 2013\Projects\MvcApplication7\MvcApplication7\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [quizsw3];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'personaSet'
CREATE TABLE [dbo].[personaSet] (
    [Id_persona] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [apellido] nvarchar(max)  NOT NULL,
    [direccion] nvarchar(max)  NOT NULL,
    [correo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'usuarioSet'
CREATE TABLE [dbo].[usuarioSet] (
    [Id_usuario] int IDENTITY(1,1) NOT NULL,
    [user_name] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_persona] in table 'personaSet'
ALTER TABLE [dbo].[personaSet]
ADD CONSTRAINT [PK_personaSet]
    PRIMARY KEY CLUSTERED ([Id_persona] ASC);
GO

-- Creating primary key on [Id_usuario] in table 'usuarioSet'
ALTER TABLE [dbo].[usuarioSet]
ADD CONSTRAINT [PK_usuarioSet]
    PRIMARY KEY CLUSTERED ([Id_usuario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------